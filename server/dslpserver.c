/*
Filename: dslpserver.c

for the time being.

TODO:
- connect the server with udp client from netcat
- exit on char+return
- new thread for every client

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
//#include <readline/readline.h>
//#include <readline/history.h>

#define MAXLINE 1024
#define MAX_SERVERNAME 50
#define MAX_CLIENT 10

void failure(char* message){
	perror(message);
	exit(1);
}

void GetServerNameFromArguments(char name[], char parsed_server_name[]){
	char tmp_name[7];
	strncpy(tmp_name,name,7);

	char server_check[] = "server=";

	if (strncmp(server_check,tmp_name,7)==0){ //check if server name properly entered
		for (int i = 8; i<MAX_SERVERNAME; i++){	//iterate over the string to get the server name
			if(name[i] == '\0'){ //check if string terminated
				break;
			}	
			parsed_server_name[i-8]=name[i];
		}
	}else{ //server input invalid
		strncpy(parsed_server_name,"notvalid",9);
		}	
}

int main(int argc, char *argv[]){

// char server_name[MAX_SERVERNAME];
   int port_num;
 //  char valid_check[] = "notvalid";
   
   if( argc == 2 ) {
      port_num = atoi(argv[1]);
      }
   else if( argc > 2 ) {
      printf("Too many arguments supplied.\n");
   }
   else {
      printf("One argument expected.\n");
   }
   
   printf("port number: %d\n", port_num);

	
  close(server_socket);

  	int lfd; // Listen File Deskriptor 
	int nfd[MAX_CLIENTS]; // Array mit FD for Clients 
	struct sockaddr_in server_address;
	struct sockaddr recieve_address;
	struct sockaddr_in client_address;
	socklen_t client_add_len;
	char client_msg[MAX_BUFFER];
	ssize_t msg_len;
	size_t msg_count = MAX_BUFFER;
	fd_set  rfds_orig, rfds_read;
	struct timeval tv;
	int retval;
	FD_ZERO(&rfds_orig);
	int fdmax;
	ssize_t sentbytes;
	int i = 0;
	ssize_t welcome_msg_len;
	ssize_t client_ip_len;
	int recieve_data_len;
	socklen_t recieve_add_len;
	ssize_t new_line_len;
	char welcome_msg[] = "\n[SERVER] Welcome to the chat! Your IP Adress is: " ;
	welcome_msg_len = sizeof(welcome_msg);
	char new_line = '\n';

	// Listen Socket Erstellen 
	if ( (lfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) { 
			failure("listen socket initialising");
		}

	printf("Listen socket: %d\n", lfd);

	// Server Adress bestimmen 
	server_address.sin_family = AF_INET;  //Domain
	server_address.sin_addr.s_addr = INADDR_ANY; // Jede kann sich mit dieser Addresse verbinden 
	server_address.sin_port = htons(port_num);
	

	// Verbinden der Socket zu einer Spezifischer Adresse und Port 
	if( bind(lfd, (struct sockaddr*) &server_address, sizeof(server_address)) == -1){
			failure("bind");
		}

	if (listen(lfd, 3)== -1){ // Die 3 hier entspricht die erlaubte Stau entspricht nicht die MAXCLIENTs 
		failure("listen");
		}	

	fdmax = lfd;
	FD_SET(lfd, &rfds_orig); // Listen Socket zur Lese File Deskriptoren hinzufuegen 
	
	while(1){
		// Zeit und Dateideskriptoren fuer Select bestimmen 
		tv.tv_sec = 9;
		tv.tv_usec = 0;
		rfds_read = rfds_orig; // Die Kopie von RFDS erstellen 
		retval = select(fdmax +1 , &rfds_read, NULL, NULL, &tv);// Wenn etwas an FD passiert  

		if (retval != 0){ 		
			for (int k =0; k<=fdmax; k++) { // Durch alle FD interrieren 
				if((FD_ISSET(k, &rfds_read))==1) { // k wird jetzt einen FD wo etwas passiert 
					if(k == lfd){
						/* wenn es bei Listen Socket passiert, dann soll es :
						 * -accept - neuen FD erstellen,
						 * -einen thread erzeugen, der bekommt FD als argument, connection thread
						 * -diese socket der neuen Client in FD_SET hinzufuegen
						 * - beobachten ob dort was passiert, und falls recvfrom -1, dann von fd set entfernen
						 * */					
						client_add_len = sizeof(client_address); 
						client_ip_len = strlen(inet_ntoa(client_address.sin_addr));
						new_line_len = sizeof(new_line);
						nfd[i] = accept(lfd,(struct sockaddr*) &client_address , &client_add_len);
						if ((sentbytes = send(nfd[i], &welcome_msg, welcome_msg_len, 0)) == -1){ 
							failure("send");
							}
						if ((sentbytes = send(nfd[i], inet_ntoa(client_address.sin_addr), client_ip_len, 0)) == -1){ 
							failure("send");
							}

						if ((sentbytes = send(nfd[i], &new_line, new_line_len, 0)) == -1){ 
							failure("send");
							}

						FD_SET(nfd[i], &rfds_orig); // Neuer client an zu der FD Set hinzufuegen 
						if (fdmax<nfd[i]) { // Falls Neue Client, FDMAX erhoehen 
							fdmax = nfd[i];
							}

						printf("New Client from %s on socket %d\n", inet_ntoa(client_address.sin_addr),nfd[i]);
						printf("New Clients port %d\n", ntohs(cli_addr[clients].sin_port));

						/*Die Maximale Anzahl der Clients beobachten */
						if (i< MAX_CLIENTS) i++;// bei nachster client einen neuen socket erstellen 
					       	else {
							failure("Too many clients\n");
								break;		
						}
					} else {											
						/* Keine neuer client, dh alte Client meldet sich, dann soll es
						* -von dieser Client Nachricht empfangen
						* -an alle andere senden	*/					
						recieve_add_len = sizeof(recieve_address); 
						if ( (msg_len = recvfrom(k, &client_msg, msg_count, 0, &recieve_address, &recieve_add_len)) <= 0){
							if (msg_len ==-1 ) failure("recv");
							if (msg_len == 0) {
								printf("Client on the socket: %d hang up\n", nfd[k]);
								close(k);
								FD_CLR(k, &rfds_orig); /* wenn Client is tot, dann es von FDSEt entfernen */
								}
							}	
						else{ 

							/* Wenn Nachricht erfolgreich empfangen ist */
							recieve_data_len = sizeof(recieve_address.sa_data);
							client_msg[msg_len] ='\0';
						       	/*Durch die Arrey mit FD iteriren, an alle absenden */
							for( int j=0; j <= fdmax; j++){	
					         		/* Senden, wenn es eine FD der Clients ist.... */
	                                    				if(FD_ISSET(j, &rfds_orig)){ 
								 	/* ...aber weder Server noch Client selbst */
								        if(j != lfd && j != k){
								/* 	Eigene Adresse an der Client senden	
									if ((sentbytes = send(j, recieve_address.sa_data , recieve_data_len, 0)) == -1){ 
									failure("send");
									}*/
									if ((sentbytes = send(j, &client_msg, msg_len+1, 0)) == -1){ 
										failure("send");
										}
									  }
									}
								} //for int j
							} //else
						}
					} //if k==lfd
				} //if FDISSET
			}

		else break;
		}

	close(lfd);

	return(0);

}	
/*
    int server_socket; //socket file descriptor
   struct sockaddr_in server_address;
   char buffer[MAXLINE];
   char *server_messagge = "Hier ist der Server";
   socklen_t len;
   int n, clients_total;
   struct sockaddr_in client_addresses[MAX_CLIENT]

   // get server Socket 
   if((server_socket = socket( AF_INET, SOCK_DGRAM, 0)) == -1) {  // Domain, Datagram, 0 fuer das standard Protokol
 	failure("socket");
   }

   printf("server socket no: %d\n", server_socket);
   memset(&server_address, 0 ,sizeof(server_address));		
	
   // set server Adsress 
   server_address.sin_family = AF_INET;  //Domain
   server_address.sin_addr.s_addr = INADDR_ANY; 
   server_address.sin_port = htons(port_num);

   // Verbinden der Socket zu einer Spezifischer Adresse und Port 
   if( bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address)) == -1){
	// Argumente: socket fd; struct mit spezieferte Adresse; Groesse von dieser Struct 
	// Bind bedeutet dass wir den Socket mit das Sistem verbinden
	failure("bind");
	}

   printf("Warten.....\n");
   fflush(stdout);
// while(1){
  //   n = recv(server_socket, (char *)buffer, MAXLINE, 0);
   if( (n = recvfrom(server_socket, (char *)buffer, MAXLINE,
		0, (struct sockaddr *) &server_address, &len)) == -1){
	//Argumente: socket fd; Puffer, wo der packet gespiechert sein soll; Groesse dieser Puffer;
	//Flags fuer Socket Beziehung; Struct mit Adresse der Quelle; Groesse dieser Struct
	//Recieve beduetet das Sistem bekommt von dieser Socket die Daten
	failure("recvfrom");
	}

   buffer[n] = '\0'; //letzte Char als neuline char stellen
   printf("client sagt: %s\n", buffer);

   // Nach dem Empfand etwas zurueck am Client senden 
   if((n = sendto(server_socket, (const char*)server_messagge, strlen(server_messagge), 
		0,   (const struct sockaddr *) &server_address, len)) == -1){
	//Argumente: socket fd; Puffer mit die Daten zu absenden; Groesse dieser Puffer;
     Flags fuer socket handlung; Struct mit Adresse der Empfaenger; Groesse dieser Struct 
	failure("sendto");
   }


   buffer[n] = '\0'; //letzte Char als neuline char stellen
   printf("Mitteilung gesendet\n");
// } //while(1)






   // get the IP address from the the host name
   int getaddr_retval;
   struct addrinfo hints;
   struct addrinfo *server_info;
   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_DGRAM;
   hints.ai_flags = AI_PASSIVE;

   if ((getaddr_retval = getaddrinfo(server_name, argv[2], &hints, &server_info) != 0)){
		   fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(getaddr_retval));
		   exit(1);
	 }

  printf("addres already there");

   int sfd; // FD fuer das Socket - so werden wir es spaeter aufrufen koennen 
   char buffer[MAXLINE];
   char *client_messagge = "Hallo, Ich bin der Client";
   struct sockaddr_in server_address;
   socklen_t len; 
   
   fd_set rfds; // Wir werden die FD auf lesen beobachten 
   fd_set wfds; // Wir werden die FD auf lesen beobachten 
   int retval; // Ruckgabewert fuer das select Funktion
   struct timeval tv; // Einer Struct die hat die Zeit Werte fuer select gespiechert 
   FD_ZERO(&rfds); // ganzen Set auf Null Stellen
   int anzahl = 3;
   int sendret;

   // Erst die Socket initialisieren  
   if ( (sfd = socket( AF_INET, SOCK_DGRAM, 0)) == -1)){  
	failure("socket");
	}
*/	
/* AF_INET ist der Domain
 * SOC_DGRAM ist einen Typ der Verbindung - hier Datagram
 0 iste fuer das standard Protokol (default)*/
   
	//GetServerNameFromArguments(argv[1], parsed_server_name);
	/*
	if(strncmp(parsed_server_name,valid_check,9)==0){
		printf("ERROR. Servername provided in the wrong form.\nExiting.");
		return 1;	
		}
	printf("Server name that has been read: %s\n",parsed_server_name);
	*/

