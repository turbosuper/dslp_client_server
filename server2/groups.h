/*  
* Filename: groups.c
*
* datastructures for the group functionality
*/

#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "groups.h"

#define MAX_GROUPS 10

typedef struct fd_node{
   int file_descriptor;
	
   struct fd_node *next;
   struct fd_node *prev;
}fd_node;

typedef struct dslp_group{
	char name[512];
	fd_node* head_of_ll;
}dslp_group;

//bool is_ll_empty();
int count_ll_nodes(fd_node* start_node);
void print_llist(fd_node* start_node);
void insert_at_start(fd_node** head_node, int fd);
struct fd_node* delete_by_fd(fd_node** head_node, int fd);

int create_group(dslp_group all_groups[], char group_name[], int *number_of_groups);
int find_group(dslp_group all_groups[], char group_name[], int *number_of_groups);
void add_group_member(dslp_group all_groups[], int file_descriptor, int group_id);
void remove_group_member(dslp_group all_groups[], int file_descriptor, int group_id);
