/* 
 * Filename: dslpproto.h
 *
 * Prototypes for the dslp protocol functions
 */

#pragma once
#include "groups.h"

int check_for_valid_first_line(char client_msg[], char msg_type[]);
int check_for_valid_dslp_body(char client_msg[]);//, char msg_type[]){
int process_msg(char msg_type[], int fd);
void clear_the_string(char string[]);
void request_time( int fd);
void group_join(char message[], int fd, int number_of_groups, dslp_group all_groups[]);
void group_leave(char message[], int fd, int number_of_groups, dslp_group all_groups[]);
void group_notify(char group_name[], char client_description[], char notify_msg[], int msg_body_lines,int fd, int number_of_groups, dslp_group all_groups[]);
void get_time_string(char time_string[]);
int parse_incoming_msgs(char client_msg[], int fd, char msg_type[], int *valid_first_line_ptr, int *valid_body_ptr, int *msg_not_valid_ptr, int *action_flag_ptr, int *body_lines_ptr, int *current_line_ptr, int number_of_groups, dslp_group all_groups[]);
void send_error_msg(char string[], int fd);
