/* 
 * Filename: groups.c
 *
* Functions to enable group communication within DSLP
* 
* The idea:
*  - groups are datatypes, that contain file descriptors with all the clients that are connected
*  - these clients are organised in a linked list (easy insert/delete possible)
*  - if necessary a message to all clients can be easily sent
* 
* So far groups are an array of fixed size.
* 
*
* */



#include "groups.h"
#define MAX_MSG_LEN 512


int count_ll_nodes(fd_node* start_node) {
   int length = 0;
   struct fd_node *current;
	
   for(current = start_node; current != NULL; current = current->next){
      length++;
   }
	
   return length;
}


void print_llist(fd_node* start_node){
	  
   printf("File desrciptors in this linked list: ");
	
   while(start_node != NULL) {        
      printf("%d, ", start_node->file_descriptor);
//      last = start_node;
      start_node = start_node->next;
   }
	
   printf(" \n");
}

//insert node at the begging
void insert_at_start(fd_node** head_node, int fd) {
   
   //create a link
   struct fd_node* new_node = (struct fd_node*) malloc(sizeof(fd_node));
   new_node->file_descriptor = fd;
   //printf("the file descritpor of the new node %p is: %d, and the old head is at %p\n",new_node, new_node->file_descriptor, head_node); 

   if((*head_node) != NULL) (*head_node)->prev = new_node;

   //place the node at the begging of the list
   new_node->next = *head_node;
   //printf("new_node->next is %p\n", new_node->next);
   new_node->prev = NULL;
  // printf("new_node->prev is %p\n", new_node->prev);

   *(head_node) = new_node;
   
}

struct fd_node* delete_by_fd(fd_node** head_node, int fd){
	struct fd_node* current = *head_node;
	struct fd_node* previous_node = NULL;

	if (*head_node == NULL){ // list is empty, can't delete
		return NULL;
	}

	while(current->file_descriptor != fd) {
	 	if(current->next == NULL){ //last element of the list, didn't find the fd
			return NULL;
		} else {
			previous_node = current;      //save the link to use later
			current = current->next;      //go to next one
			}
	}
	
	//node has been found
	//swap the links
	if(current == *head_node){  //it is the first node
		if(current->next == NULL){ //it is aslo the last node - it is the only one left
			//	printf("current node, with fd %d, has no link to prev. is this the last and only?", current->file_descriptor);
				*head_node = NULL;
			}else{ //it was not the last one
			*head_node = current->next; //set the new head node
			(*head_node)->prev = NULL;
		}
	}else{ //it is not the first node
		if(current->next == NULL){ //it is the last node
			current->prev->next= NULL;				   
		}else{ // not the last, some in the middle
			current->next->prev = current->prev; //point to the previous one
			current->prev->next = current->next; //point to the next one
		}
	}	
	return current; //this node hase been removed. 
}

int create_group(dslp_group all_groups[], char group_name[], int *number_of_groups){
//	printf("Creating the %s group\n", group_name);

	if(*number_of_groups >= MAX_GROUPS) return 1; 		//too many groups. not creating.
	strcpy(all_groups[*number_of_groups].name,group_name);   //set the name of the group
	all_groups[*number_of_groups].head_of_ll = NULL;

	 (*number_of_groups)++;					//increase the total group number

return 0;	
}

/*
* function that looks for a group with a given name
 */
int find_group(dslp_group all_groups[], char group_name[], int *number_of_groups){
	/*wierd string termination bug*/
	char group_name_str[MAX_MSG_LEN];
	int string_end = strlen(group_name);
	if(string_end==2){
		strncpy( group_name_str, group_name, string_end);
		 group_name_str[string_end-1] = '\0';
	}else{
		strncpy( group_name_str, group_name, string_end-1);
		 group_name_str[string_end] = '\0';
		}

//	printf("the group I am looking for: %s, groups total %d\n", group_name, *number_of_groups);
	for(int k = 0;k < (*number_of_groups); k++){
		if(strcmp(all_groups[k].name,group_name)==0) return k;
	}
	return -1;
}

/*
 * Function that adds members to the group
 *
 * The group needs to exist before adding a member.
 */
void add_group_member(dslp_group all_groups[], int file_descriptor, int group_id){
	
   	insert_at_start(&(all_groups[group_id].head_of_ll), file_descriptor);

}

/*
 * Function that removes member from the group
 *
 * The group needs to exist before adding a member.
 */
void remove_group_member(dslp_group all_groups[], int file_descriptor, int group_id){
	
   	delete_by_fd(&(all_groups[group_id].head_of_ll), file_descriptor);
}

