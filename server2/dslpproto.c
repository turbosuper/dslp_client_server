/*
 * Filename: dslpproto.c
 *
 * Functions to implement the behaviour cmpliant with dslp 4.0
 */ 
#include <stdio.h>
#include <string.h>
#include "dslpproto.h"
#include <sys/socket.h>
#include <time.h>
#include <stdlib.h>
//#include "groups.h"

#define MAX_MSG_LEN 512

/* Function that parses the incoming messages
 *
 * Function that reads incoming messages line by line, and checks for correct
 * header and message integrity. Function is called every time a server recieves
 * a message ending with a \n
 *
 * param client_msg - received message from client
 * param fd - file descriptor of the connection
 * param msg_type - string that contains the message type
 * param *valid_first_line - pointer to a variable that stores information,
 * 			     if the first line is formatted properly
 * param *valid_header - pointer to a variable that stores information if the header is valid
 * param *valid_body - pointer to a variable that stores information if the body is valid
 * param *line_count
 * param *body_lines - pointer that keeps track how many, of the supposed lines have been recieved
 * action_flags:
 * 1 - group join
 * 2 - group leave
 * 3 - group notify
 */
int parse_incoming_msgs(char client_msg[], int fd, char msg_type[], int *valid_first_line_ptr, int *valid_body_ptr, int *line_count_ptr, int *action_flag_ptr, int *body_lines_ptr,int *current_line_ptr, int number_of_groups, dslp_group all_groups[]){
	
	char no_dslp_body[] = "message does not comply to the protocol - missing 'dslp-body' ";
	char not_implemented[] = "this type of operation has not been yet implemented ";
	char group_name[MAX_MSG_LEN], description[MAX_MSG_LEN], notify_msg[3*MAX_MSG_LEN]; 	//TODO: the notify message can be very long should be placed on heap,
	
	if(*line_count_ptr == 0){ //start all over 
		*valid_first_line_ptr= check_for_valid_first_line(client_msg, msg_type);        //first check - for 'dslp-4.0/' at the beginning of the first line
	//	printf("valid_first_line pointer value is: %d\n", *valid_first_line_ptr);             
		if (*valid_first_line_ptr != 1){ 
			send_error_msg(client_msg, fd); *line_count_ptr = 0; 			//if message doesn not have it, send error
			}else{ 
			(*line_count_ptr)++; *valid_first_line_ptr = 0;}			//if first part of header valid, increase the line count, reset the first check
		return 0;
		}

	else if(*line_count_ptr == 1){ 								//if first part of the first line fits the protocol, then check the message type
		if (strncmp(msg_type,"request time",12) == 0){ 					//the 'time request' message type had to recieve now the 'dslp-body' line to proceed
			printf("string recieved from client on fd: %d is: %s",fd, client_msg);
	       		*valid_body_ptr = check_for_valid_dslp_body(client_msg);
//			printf("valid body ptr (check for dslp-body) is %d.\n", *valid_body_ptr);
			if(*valid_body_ptr == 1){						// the 'dslp-body' received, send response, reset the check for 'dslp-body' and line count		
				request_time(fd); *valid_body_ptr = 0;
			}else{ 
				send_error_msg(no_dslp_body, fd);}				//otherwise, send an error that no dslp body is there		
			*line_count_ptr = 0;
			*valid_body_ptr = 0;

		}else if(strncmp(msg_type,"group join",10) == 0){				//the 'group join' message type requires one additional line to read before the 'dslp-body'	
			strcpy(group_name,client_msg);						//save the group name, that comes with this line
			*line_count_ptr =2;							
			*action_flag_ptr = 1;							//mark which action should be taken once the dslp-body comes
			printf("client %d wants to join the group called: %s\n", fd, group_name);
			
		}else if(strncmp(msg_type,"group leave",12) == 0){				//the 'group leave' message type requires one additional line to read before the 'dslp-body'	
			strcpy(group_name,client_msg);						//save the group name
			*line_count_ptr =2;							
			*action_flag_ptr = 2;							//mark which action should be taken once the dslp-body comes
			printf("client %d wants to leave the group called: %s\n", fd, group_name);
			
		}else if(strncmp(msg_type,"group notify",12) == 0){				//the 'group notify' message requires: 3 lines before the 'dslp-body', and then
												//					variable number of lines	
			strcpy(description,client_msg);						//this line has the client description, save it
			*line_count_ptr = 2;							
			*action_flag_ptr = 3;							//mark which action should be taken once the dslp-body comes
			printf("the client descritpion is: %s\n",description);
			}
		else{send_error_msg(not_implemented, fd), *line_count_ptr = 0;}			//for all the other cases, that have not been implemented yet
		}

	else if(*line_count_ptr == 2){								//for message type with header that is at least 3 lines long
		if(*action_flag_ptr == 1){							//if 'group join', then check for 'dslp-body' before processing
	       		*valid_body_ptr = check_for_valid_dslp_body(client_msg);
		//	printf("valid body ptr (check for dslp-body) is %d.\n", *valid_body_ptr);
			if(*valid_body_ptr == 1){						// the 'dslp-body' received, process the join message, reset the check for 'dslp-body' and line count		
				printf("the client %d will join the group %s\n", fd, group_name);
				group_join(group_name, fd, number_of_groups,all_groups); *valid_body_ptr = 0;
			}else{ 
				send_error_msg(no_dslp_body, fd);}				//otherwise, send an error that no dslp body is there		
			*line_count_ptr = 0;
			*valid_body_ptr = 0;
			*action_flag_ptr = 0;							//done with this message type, reset the flag
		}
		if(*action_flag_ptr == 2){							//if 'group leave', then check for 'dslp-body' before processing
	       		*valid_body_ptr = check_for_valid_dslp_body(client_msg);
		//	printf("valid body ptr (check for dslp-body) is %d.\n", *valid_body_ptr);
			if(*valid_body_ptr == 1){						// the 'dslp-body' received, process the leave message, reset the check for 'dslp-body' and line count		
				printf("the client %d will leave the group %s\n", fd, group_name);
				group_leave(group_name,fd, number_of_groups,all_groups); *valid_body_ptr = 0;
			}else{ 
				send_error_msg(no_dslp_body, fd);}				//otherwise, send an error that no dslp body is there		
			*line_count_ptr = 0;
			*valid_body_ptr = 0;
			*action_flag_ptr = 0;							//done with this message type, reset the flag
		}
		if(*action_flag_ptr == 3){							//if 'group notify', then expect the name of the group now
			strcpy(group_name,client_msg);						//save the name of the group
			*line_count_ptr = 3;							
			*action_flag_ptr = 3;						
			printf("the group to notify is: %s\n",group_name);
			}
	} // else line_count_ptr ==2

	else if(*line_count_ptr == 3){		
		if(*action_flag_ptr == 3){							//if 'group notify', then expect the number of lines for the message 
			*body_lines_ptr = atoi(client_msg);					//(currently stack used for this, heap would be better) 
			printf("the notify message will be %d lines long.",*body_lines_ptr);
			*line_count_ptr=4;
		}else{										//in case something went wrong, should not come to this. reset the values
			*line_count_ptr=0;
			*valid_body_ptr=0;
			*valid_first_line_ptr=0;
			*action_flag_ptr=0;
		}	
	} // else line_count_ptr == 4
	
	else if(*line_count_ptr == 4){		
		if(*action_flag_ptr == 3){							//if 'group notify', then expect the dslp-body now
	       		*valid_body_ptr = check_for_valid_dslp_body(client_msg);
		//	printf("valid body ptr (check for dslp-body) is %d.\n", *valid_body_ptr);
			if(*valid_body_ptr == 1){						// the 'dslp-body' received, send response, reset the check for 'dslp-body' and line count		
				//group_notify(group_name, description, *body_lines_ptr, fd); 
				*valid_body_ptr = 0;
				*valid_first_line_ptr = 0;
			}else{ 
				send_error_msg(no_dslp_body, fd);				//otherwise, send an error that no dslp body is there
				*line_count_ptr=0;
				*valid_body_ptr=0;
				*valid_first_line_ptr=0;
				*action_flag_ptr=0;
				return 0;
				}			

			*line_count_ptr=5;
			return 0;
		}else{										///in case something went wrong, should not come to this. reset the values
			*line_count_ptr=0;
			*valid_body_ptr=0;
			*valid_first_line_ptr=0;
			*action_flag_ptr=0;
		}	
	} //else line_count_ptr ==4
	
	else if(*line_count_ptr >= 5){								//there can be many lines that come here
		printf("line count over 5.\n");
		if(*action_flag_ptr == 3){							//for the 'group notify' message type aggragate the messages in a single char
			printf("variable check: current_line is: %d, body_lines var: %d\n", *current_line_ptr, *body_lines_ptr);
			while(*current_line_ptr<*body_lines_ptr-1){					//TODO: split them into separate strings
				strcat(notify_msg, client_msg);
				//printf("for-loop | The string to save is: %s, the full notify msg is: %s", client_msg, notify_msg);
				(*current_line_ptr)++;
				return 0;
				}
			strcat(notify_msg, client_msg);
			group_notify(group_name, description, notify_msg, *body_lines_ptr, fd, number_of_groups, all_groups); 
		//	printf("this string will get sent: %s", notify_msg);
			clear_the_string(notify_msg);
			*current_line_ptr = 0;							//when done, reset the values
			*body_lines_ptr = 0;
			*line_count_ptr=0;
			*valid_body_ptr=0;
			*valid_first_line_ptr=0;
			*action_flag_ptr=0;
			return 0;
		}else{										//this msg type does not need to read so many lines, should not come to this. reset the values
			*line_count_ptr=0;
			*valid_body_ptr=0;
			*valid_first_line_ptr=0;
			*action_flag_ptr=0;
		}	
	} //else line_count_ptr >=5

	return 0;
}
		

/* function that checks if the received string is a valid protocol messagge,
 * parses the string to extract message type
 * 
 * param client_msg - received message from client
 * param msg_type - message type
 *
 * ret 1 if correct dslp header recognised, 0 otherwise
 */
int check_for_valid_first_line(char client_msg[], char msg_type[]){
	int msg_len;
	msg_len = strlen(client_msg);
	char dslp_head[] = "dslp-4.0/";					
	//check for header, save the message type
	if (strncmp(client_msg,dslp_head,9)==0){ //check if protocol name properly entered
		for (int i = 9; i<msg_len; i++){	//iterate over the string to save the msg type
			msg_type[i-9]=client_msg[i];
			}
		msg_type[msg_len-10] = '\0';
		printf("proper dslp header found! dslp message type: %s (no new line should be here).\n",msg_type);
	}else{ //not a proper dslp header
		printf("not a dslp message.\n");
		return 0;
		}	
	return 1;
}
/* Function that checks if a message has a 'dslp-body' string in it
 *
 */
int check_for_valid_dslp_body(char client_msg[]){ 
	char dslp_body[] = "dslp-body";
	if (strncmp(client_msg,dslp_body,9)==0){
		return 1;
		}
	return 0;
}

/* function that sends an error message
 */
void send_error_msg(char string[], int fd){
	
        int sent, error_str_len;
	//get the ridicoulus newline bug out
	int string_end;
	char wrong_command[MAX_MSG_LEN];
	string_end = strlen(string);
	if(string_end==2){
		strncpy(wrong_command, string, string_end);
		wrong_command[string_end-1] = '\0';
	}else{
		strncpy(wrong_command, string, string_end-1);
		wrong_command[string_end] = '\0';
		}
	
	char error_str[2*MAX_MSG_LEN];
       	sprintf(error_str,"dslp-4.0/error\n2\ndslp-body\nCannot process  '%s'. Not a correct DSLP 4.0 message.\nPlease consult the DSLP 4.0 specification for a list of valid message types.\n", wrong_command);
	error_str_len = strlen(error_str);
	if ((sent = send(fd, &error_str, error_str_len+1, 0)) == -1){ 	
		printf("sending failed");	
	}
}

int process_msg(char msg_type[], int fd){
	printf("message of type '%s' will be processed.\n Processing...\n", msg_type);
	if (strncmp(msg_type,"request time",12) == 0){
		request_time(fd);
		}
	else{
		send_error_msg(msg_type, fd);
		}
	
	return 0;
}

void clear_the_string(char string[]){
	for (int i=0; i < MAX_MSG_LEN; i++){
		string[i] = '\0';
		}
}

void request_time(int fd){
	int sent;
	char msg_from_func[] = "dslp-4.0/response time\ndslp-body\n";
	int this_msg_len = strlen(msg_from_func);
	char time_str[25];	
	get_time_string(time_str);
      	if ((sent = send(fd, &msg_from_func, this_msg_len+1, 0)) == -1){ 	
		printf("sending failed");	
       	   }
	
	this_msg_len = strlen(time_str);
      	if ((sent = send(fd, &time_str, this_msg_len+1, 0)) == -1){ 	
		printf("sending failed");	
       	   }
}

void get_time_string(char time_string[]){
	time_t raw_time;
	struct tm *time_info;
	time(&raw_time);
	time_info = gmtime(&raw_time);
	printf("in func: %d-%02d-%02d %02d:%02d:%02d",(time_info->tm_year)+1900, (time_info->tm_mon)+1,time_info->tm_mday, 
			time_info->tm_hour, time_info->tm_min,time_info->tm_sec);
	sprintf(time_string,"%d-%02d-%02d %02d:%02d:%02d\n",(time_info->tm_year)+1900, (time_info->tm_mon)+1,time_info->tm_mday, 
			(time_info->tm_hour)+2, time_info->tm_min,time_info->tm_sec);
	//yyyy-MM-dd HH:mm:ss	
}


void group_join(char message[], int fd,int number_of_groups, dslp_group all_groups[]){
	int sent;
	char group_name[MAX_MSG_LEN];
	char send_back_msg[2*MAX_MSG_LEN];
	int group_id;

	/*wierd string termination bug*/
	char group_name_str[MAX_MSG_LEN];
	int string_end = strlen(message);
	if(string_end==2){
		strncpy( group_name_str, message, string_end);
		 group_name_str[string_end-1] = '\0';
	}else{
		strncpy( group_name_str, message, string_end-1);
		 group_name_str[string_end] = '\0';
		}

	printf("the dslp-body arrived, ready to add to the group %s.\n",group_name);
	sprintf(send_back_msg,"dslp-4.0/group join ack\n%s\ndslp-body\n",group_name_str);
	int msg_len =strlen(send_back_msg);
	if ((sent = send(fd, &send_back_msg, msg_len+1, 0)) == -1){ 	
		printf("sending failed");	
	   }
	//join the group: 
	//search if the group exists, and create it, when not
	group_id = find_group(all_groups, group_name_str, &number_of_groups);
	if( group_id== -1){
//		printf("cannot find the group. creating %s",group_name_str);
   		create_group(all_groups, group_name_str, &number_of_groups);
		}
	
	//add the fd to the group.
 	 add_group_member(all_groups, fd, group_id);	
	printf("client added. Clients currently in %s group: \n",group_name_str);
	print_llist(all_groups[group_id].head_of_ll);	
}

void group_leave(char message[], int fd, int number_of_groups, dslp_group all_groups[]){
	int sent;
	char group_name[MAX_MSG_LEN];
	char send_back_msg_succ[2*MAX_MSG_LEN];
	char send_back_msg_fail[2*MAX_MSG_LEN];
	int group_id;

	strcpy(group_name,message);
//	printf("the dslp-body arrived, ready to add to the group %s.\n",group_name);
	sprintf(send_back_msg_succ,"dslp-4.0/group leave ack\n%sdslp-body\n",group_name);
	sprintf(send_back_msg_fail,"dslp-4.0/group leave ack\n%s\n1\ndslp-body\nFailed to leave. Given group doesn't exist.\n",group_name);
	int msg_len_succ =strlen(send_back_msg_succ);
	int msg_len_fail =strlen(send_back_msg_fail);
	//leaving the group:
	//search for the group 
	group_id = find_group(all_groups, group_name, &number_of_groups);
	if( group_id== -1){
		printf("group doesn't exisit. check for tipping mistakes '%s'", group_name);
		if ((sent = send(fd, &send_back_msg_fail, msg_len_fail+1, 0)) == -1){ //send ack with failure msg	
			printf("sending failed");	
		   }
		}else{ //group exists, leav it
			remove_group_member(all_groups, fd, group_id);	
			if ((sent = send(fd, &send_back_msg_succ, msg_len_succ+1, 0)) == -1){ //send ack with succ msg	
				printf("sending failed");	
			   }
			printf("client removed. Clients currently in %s group: \n",group_name);
			print_llist(all_groups[group_id].head_of_ll);	
		}
	
}

void group_notify(char group_name[], char client_description[], char notify_msg[], int msg_body_lines,int fd, int number_of_groups, dslp_group all_groups[]){ 
	printf("in the group notify function.");
	int sent;
	//create the message to send
	char group_msg[4*MAX_MSG_LEN];
	sprintf(group_msg, "dslp-4.0/group notify\n%s%s%d\ndslp-body\n%s", client_description, group_name, msg_body_lines, notify_msg);
	int msg_len = strlen(group_msg);
	
	//find the group id
	int group_id;
        int cfd; //file descriptors for the clients	
	group_id= find_group(all_groups, group_name, &number_of_groups);
			print_llist(all_groups[group_id].head_of_ll);	

	
	//iterate over the file descriptors and send the message
	fd_node* node_ptr = all_groups[group_id].head_of_ll;		
	while(node_ptr != NULL){
		cfd = node_ptr->file_descriptor;
		printf("the file desciprotrs i found is: %d\n", cfd);
		if ((sent = send(cfd, &group_msg, msg_len+1, 0)) == -1){ 
				printf("sending failed");	
			   }
		node_ptr = node_ptr->next;
	}

       printf("the group name is %s, the client description: %s, message is %d lines long, and the message is: %s.\n", group_name, client_description, msg_body_lines, notify_msg);
}

