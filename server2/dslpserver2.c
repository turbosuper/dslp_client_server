/*
* Filename: dslpserver2.c
* 
* File consist the main server functionality: listening, accepting, and connecting.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "dslpproto.h"
#include "groups.h"
#include <stdbool.h>

#define MAXLINE 1024
#define MAX_SERVERNAME 50
#define MAX_CLIENTS 10
#define MAX_BUFFER 512 

pthread_t tid[2];
int nfd[MAX_CLIENTS];

dslp_group all_groups[MAX_GROUPS]; //array for ten groups
int number_of_groups;


typedef struct accept_thread_data{
	int fd;
	struct sockaddr client_address;
	socklen_t client_add_len;
}accept_thread_data;

void failure(char* message){
	perror(message);
	exit(1);
}
void* connection_thread(void *accfd){
	char msg_type[MAX_BUFFER];
	int accepted_fd = *(int *)accfd;
	printf("Connection thread started. connected on socket: %d\n", accepted_fd);
	ssize_t welcome_msg_len;		
	ssize_t sentbytes;
	char welcome_msg[] = "\n[SERVER] Welcome to the DSLP server.\n" ;
	welcome_msg_len = sizeof(welcome_msg);
	//client_add_len = sizeof(client_address);
	if ((sentbytes = send(accepted_fd, &welcome_msg, welcome_msg_len, 0)) == -1){ 
			failure("send");
		}
	//receive data
	ssize_t msg_len;
	char client_msg[MAX_BUFFER];
	struct sockaddr recieve_address;	
	socklen_t recieve_add_len;
	size_t msg_count = MAX_BUFFER;

	int valid_first_line, valid_body, line_count, action_flag, body_lines_num, current_line;
	int *valid_first_line_ptr, *valid_body_ptr, *line_count_ptr, *action_flag_ptr, *body_lines_num_ptr, *current_line_ptr;
	valid_first_line_ptr = &valid_first_line;
	valid_body_ptr = &valid_body;
	line_count_ptr= &line_count;
	action_flag_ptr = &action_flag;
	body_lines_num_ptr = &body_lines_num;
	current_line_ptr = &current_line;
	*valid_first_line_ptr = 0;
	*valid_body_ptr = 0;
	*line_count_ptr = 0;
	*action_flag_ptr = 0;
	*body_lines_num_ptr = 0;
	*current_line_ptr = 0;

	while(1){ //always recieve
	if ( (msg_len = recvfrom(accepted_fd, &client_msg, msg_count, 0, &recieve_address, &recieve_add_len)) <= 0){
		if (msg_len ==-1 ) failure("recv");
		if (msg_len == 0) {
		   printf("Client on the socket: %d hang up\n", accepted_fd);
		   close(accepted_fd);
		   break;
		}
	    }else{ 
	       printf("\n\ndata recieved\n");
	       parse_incoming_msgs(client_msg, accepted_fd, msg_type, valid_first_line_ptr,  valid_body_ptr, line_count_ptr, action_flag_ptr, body_lines_num_ptr, current_line_ptr, number_of_groups, all_groups);
	   }

	clear_the_string(client_msg);
	}//while(1)	
}

void* accept_connection(void* lfd){ //"server thread"
	
	struct sockaddr_in client_address;
	socklen_t client_add_len;
	int connection_fd = *(int *)lfd;
	int fd_num = 0;
	printf("Messagge from a server thread. Accepting on socket: %d.\n", connection_fd);
	while(1){
		nfd[fd_num] = accept(connection_fd,(struct sockaddr*) &client_address , &client_add_len);
		printf("Accepted connection, new fd number: %d, creating connection thread.\n",nfd[fd_num]);
	
   		int err = pthread_create(&(tid[1]), NULL, &connection_thread,&nfd[fd_num]);
        	if (err != 0){
			printf("\ncan't create thread :[%s]", strerror(err));
		}else{
			printf("connection thread crated.\n");
		}
		//check if possible to generate more connections
		fd_num++;
		if (fd_num >= MAX_CLIENTS){
			printf("too many clients, will not accept anymore\n");
			break;
			}
		//}//else
	} //while(1)
}

int test_linked_lists(){

   //groups struct linked list tests
   printf("**************** linked list testing************\n");
   fd_node* head_of_group1 = NULL;
   insert_at_start(&head_of_group1,15);
   insert_at_start(&head_of_group1,225);
   insert_at_start(&head_of_group1,373737);
   insert_at_start(&head_of_group1,5);
   print_llist(head_of_group1);

   printf("trying to delete non existing node...\n Result is: ");
   delete_by_fd(&head_of_group1, 129);
   print_llist(head_of_group1);		
   printf("trying to delete 5, the first node...\n Result is: ");
   delete_by_fd(&head_of_group1, 5);
   print_llist(head_of_group1);		
   printf("trying to delete 15, the last node...\n result is: ");
   delete_by_fd(&head_of_group1, 15);
   print_llist(head_of_group1);		
   printf("trying to delete 373737 node...\n result is: ");
   delete_by_fd(&head_of_group1, 373737);
   print_llist(head_of_group1);		
   printf("trying to delete 373737 node...\n result is: ");
   delete_by_fd(&head_of_group1, 373737);
   print_llist(head_of_group1);		
   printf("trying to delete 225 remaining node...\n result is: ");
   delete_by_fd(&head_of_group1, 225);
   print_llist(head_of_group1);		
   printf("trying to delete from list that has been emptied...\n result is: ");
   delete_by_fd(&head_of_group1, 373737);
   print_llist(head_of_group1);		
   printf(" putting few new variabnlesto the list that has been emptied...\n result is: ");
   insert_at_start(&head_of_group1,13);
   insert_at_start(&head_of_group1,285);
   insert_at_start(&head_of_group1,36);
   insert_at_start(&head_of_group1,32);
   insert_at_start(&head_of_group1,4);
   print_llist(head_of_group1);	

   printf("trying to delete 13, the last node...\n result is: ");
   delete_by_fd(&head_of_group1, 13);
   print_llist(head_of_group1);	

   printf("trying to delete 32 ...\n result is: ");
   delete_by_fd(&head_of_group1, 32);
   print_llist(head_of_group1);	

   printf(" putting few other variables to the exsiting list ...\n result is: ");
   insert_at_start(&head_of_group1,12);
   insert_at_start(&head_of_group1,28);
   insert_at_start(&head_of_group1,2);
   print_llist(head_of_group1);	

   printf("trying to delete 2 the first node ...\n result is: ");
   delete_by_fd(&head_of_group1, 2);
   print_llist(head_of_group1);	

   printf(" putting few other variables to the exsiting list ...\n result is: ");
   insert_at_start(&head_of_group1,22);
   insert_at_start(&head_of_group1,7);
   print_llist(head_of_group1);	

   printf("**************** End of linked list testing************\n");

   printf("=================== testing groups======================*\n");
   int group_num = 0;
   char test_name[] = "test group1";
   char test_name2[] = "test group2";
   char test_name3[] = "test group3";
   char test_name4[] = "test group4";

   printf("trying to create a group\n");

   create_group(all_groups, test_name, &group_num);
   printf("Group %s created.\n", all_groups[0].name);

   create_group(all_groups, test_name2, &group_num);
   printf("Group %s created.\n", all_groups[1].name);
   
   create_group(all_groups, test_name3, &group_num);
   printf("Group %s created.\n", all_groups[2].name);
   
   create_group(all_groups, test_name4, &group_num);
   printf("Group %s created.\n", all_groups[3].name);

   int group_id = find_group(all_groups, test_name2, &group_num); 	
   printf("Group %s found.\n",  all_groups[group_id].name);

   printf("trying to find a groupt that doesn'exist returns:");
   group_id = find_group(all_groups, "not existing name", &group_num); 	
   printf(" %d\n", group_id);

   printf("adding few members to one of the group\n");
  add_group_member(all_groups, 3, 1);
  add_group_member(all_groups, 4, 1);
  add_group_member(all_groups, 17, 1);
  add_group_member(all_groups, 23, 1);

   print_llist(all_groups[1].head_of_ll);	
   
   printf("testing if removing members from group works.\n");

   remove_group_member(all_groups, 4, 1);
   remove_group_member(all_groups, 23, 1);

   print_llist(all_groups[1].head_of_ll);	
	return 0;
}

int main(int argc, char *argv[]){
   int port_num;
   number_of_groups = 0;

   if( argc == 2 ) {
      port_num = atoi(argv[1]);
      }
   else if( argc > 2 ) {
      printf("Too many arguments supplied.\n");
   }
   else {
      printf("One argument expected.\n");
   } 
   printf("port number: %d\n", port_num);

   int lfd;
   int nfd[MAX_CLIENTS];
   struct sockaddr_in server_address;

   //test_linked_lists();  //uncomment for a quick check of linked list and groups

   printf("\n\n");
   
  //start listen socket 
  if ( (lfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) { 
		failure("listen socket initialising");
	}
	printf("Listen socket: %d\n", lfd);

	//set server address 
	server_address.sin_family = AF_INET;  //Domain
	server_address.sin_addr.s_addr = INADDR_ANY; //everybody can connect to the address 
	server_address.sin_port = htons(port_num);
	//bind the socket to the port and anddress 
	if( bind(lfd, (struct sockaddr*) &server_address, sizeof(server_address)) == -1){
			failure("bind");
		}

	if (listen(lfd, 3)== -1){ // this '3' means allowed traffic, not max clients 
		failure("listen");
		}	

	// start the thread to accept connection. further thread will get started from this thread,
	// when accept returns succesfully
   	int err = pthread_create(&(tid[0]), NULL, &accept_connection,&lfd); 
        if (err != 0){
            printf("\ncan't create thread :[%s]", strerror(err));
	}

	while (fgetc(stdin) != EOF)
		exit(EXIT_SUCCESS);

	return(0);
}
